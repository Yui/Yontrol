import React, { useState, useEffect } from 'react';
import useWebSocket, { ReadyState } from "react-use-websocket";
import { useRef } from 'react';

const MousePad = () => {
  const [isMouseDown, setIsMouseDown] = useState(false);
  const [startPosition, setStartPosition] = useState({ x: 0, y: 0 });
  const [sensitivity, setSensitivity] = useState(5);
  const [wsPath, setWsPath] = useState('');

  const mousepadRef = useRef(null);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const fixedProtocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
      const origin = window.location.origin.replace(window.location.protocol, fixedProtocol);
      const parsedPath = `${origin}/ws`;
      if (wsPath !== parsedPath) setWsPath(parsedPath);
    }
  }, []);

  const { sendJsonMessage, readyState } = useWebSocket(wsPath, {
    onOpen: () => console.log(`WS Connected: ${wsPath}`),
    share: true,
    filter: () => false,
    retryOnError: true,
    shouldReconnect: () => true
  });

  const handleTouchStart = (event) => {
    event.preventDefault(); // Prevent default browser behavior (text selection)
    setIsMouseDown(true);
    setStartPosition({ x: event.touches[0].clientX, y: event.touches[0].clientY });
  };

  const handleTouchMove = (event) => {
    if (!isMouseDown) return; // Ignore move events if not pressed

    const { clientX, clientY } = event.touches[0];
    const deltaX = (clientX - startPosition.x) * sensitivity;
    const deltaY = (clientY - startPosition.y) * sensitivity;

    // Update startPosition for continuous tracking
    setStartPosition({ x: clientX, y: clientY });

    console.log("sending", { eventType: "mousepad", x: deltaX, y: deltaY });
    sendJsonMessage({ eventType: "mousepad", x: deltaX, y: deltaY });
  };

  const handleTouchEnd = () => {
    setIsMouseDown(false);
  };

  const handleSensitivityChange = (event) => {
    setSensitivity(parseInt(event.target.value));
  };

  const handleClick = (type) => {
    console.log("sending", { eventType: "mouse-click", btn: type });
    sendJsonMessage({ eventType: "mouse-click", btn: type });
  };

  const handleHold = (type, status) => {
    console.log("sending", { eventType: "mouse-hold", btn: type, status });
    sendJsonMessage({ eventType: "mouse-hold", btn: type, status });
  };

  return (
    <div className="container">
      <div ref={mousepadRef} className="mousepad" onTouchStart={handleTouchStart} onTouchMove={handleTouchMove} onTouchEnd={handleTouchEnd}>
        {/* Add your mousepad design here */}
      </div>

      <div className="button-rows">
        <div className="button-row">
          <button onClick={() => handleClick("left")}>Left Click</button>
          <button onClick={() => handleClick("right")}>Right Click</button>
        </div>
        <div className="button-row">
          <button onClick={() => handleHold("left", true)}>Left Hold On</button>
          <button onClick={() => handleHold("right", true)}>Right Hold On</button>
        </div>
        <div className="button-row">
          <button onClick={() => handleHold("left", false)}>Left Hold Off</button>
          <button onClick={() => handleHold("right", false)}>Right Hold Off</button>
        </div>
      </div>        

      <div className="slider-container">
        <label htmlFor="sensitivity">Sensitivity:</label>
        <input type="range" id="sensitivity" min="1" max="10" onChange={handleSensitivityChange} defaultValue={sensitivity} />
      </div>
    </div>
  );
};

export function App(props) {
  return (
    <div className='App'>
      <MousePad></MousePad>
    </div>
  );
}