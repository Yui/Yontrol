#!/bin/bash
cd /workdir

# Define a function to display help
function help {
  echo "Usage: $0 [--build] [--run]"
  echo "  --build: Build something (specific actions here)"
  echo "  --run: Run something (specific actions here)"
  echo "  No arguments: Display this help message"
}

# Check for arguments
if [[ $# -eq 0 ]]; then
  help
  exit 1
fi

# Process arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    --build)
      echo "Building..."
      npm install
      npx react-scripts build
      ;;
    --run)
      echo "Running..."
      npm install
      sh -c "BROWSER=none npx react-scripts start"
      ;;
    *)
      echo "Error: Unknown argument '$1'"
      help
      exit 1
      ;;
  esac
  shift
done

