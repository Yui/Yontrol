//go:build windows

package mouse

import (
	"fmt"
	"io"
	"os/exec"
	"sync"
)

var (
	psStdIn      io.WriteCloser
	psStdinMutex sync.Mutex
	psCmd        exec.Cmd

	writeCommand = func(cmd string) error {
		psStdinMutex.Lock()
		defer psStdinMutex.Unlock()

		_, err := io.WriteString(psStdIn, cmd+"\n")
		psCmd.Wait()

		return err
	}
)

func StartPowerShell() error {
	psCmd := exec.Command("powershell.exe", "-nologo", "-noprofile")

	var err error

	psStdIn, err = psCmd.StdinPipe()

	if err != nil {
		return err
	}

	if err = psCmd.Start(); err != nil {
		fmt.Println("An error occured: ", err)
	}

	return nil
}

func WritePowerShell(cmd string) error {
	return writeCommand(cmd)
}
