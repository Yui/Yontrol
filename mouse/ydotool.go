//go:build linux

package mouse

import (
	"os/exec"
	"strconv"
)

/*
*
Click mouse using ydotool on linux

0 - left
1 - right
40 - left down
80 - left up
41 - right down
81 - right ups
*/
func ClickMouseYDO(btn int) error {
	//fmt.Println("Clicking", btn)
	e := exec.Command("ydotool", "click", "0xC"+strconv.Itoa(btn))
	_, err := e.CombinedOutput()

	return err
}

func MoveMouseRelativeYDO(x int, y int) error {
	//fmt.Println("moving mouse using ydo", x, y)
	e := exec.Command("ydotool", "mousemove", "-x", strconv.Itoa(x), "-y", strconv.Itoa(y))
	_, err := e.CombinedOutput()

	return err
}

func TypeTextYDO(text string) error {
	//fmt.Println("typing text using ydo", text)
	e := exec.Command("ydotool", "type", "--", text)
	_, err := e.CombinedOutput()

	return err
}

func TypeSpecialYDO(text []string) error {
	//fmt.Println("ydotool", append([]string{"key", "--"}, text...))
	e := exec.Command("ydotool", append([]string{"key", "--"}, text...)...)
	_, err := e.CombinedOutput()

	return err
}
