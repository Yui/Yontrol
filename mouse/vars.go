package mouse

type MouseInstruction int64

const (
	LeftBtn      MouseInstruction = 0
	RightBtn     MouseInstruction = 1
	LeftBtnDown  MouseInstruction = 2
	LeftBtnUp    MouseInstruction = 3
	RightBtnDown MouseInstruction = 4
	RightBtnUp   MouseInstruction = 5
)

type SpecialButtons int64

const MaxSpecialButtons int = 10

const (
	WinBtn    SpecialButtons = 0
	CtrlBtn   SpecialButtons = 1
	AltBtn    SpecialButtons = 2
	TabBtn    SpecialButtons = 3
	ArrLeft   SpecialButtons = 4
	ArrUp     SpecialButtons = 5
	ArrRight  SpecialButtons = 6
	ArrDown   SpecialButtons = 7
	BackSpace SpecialButtons = 8
	Enter     SpecialButtons = 9
)
