//go:build windows

package mouse

import (
	"fmt"
	"syscall"
	"unsafe"
)

// Mouse event constants
const (
	MOUSEEVENTF_LEFTDOWN   = 0x0002
	MOUSEEVENTF_LEFTUP     = 0x0004
	MOUSEEVENTF_RIGHTDOWN  = 0x0008
	MOUSEEVENTF_RIGHTUP    = 0x0010
	MOUSEEVENTF_MIDDLEDOWN = 0x0020
	MOUSEEVENTF_MIDDLEUP   = 0x0040
	MOUSEEVENTF_ABSOLUTE   = 0x8000
)

// Keyboard event constants
const (
	KEYEVENTF_EXTENDEDKEY = 0x0001
	KEYEVENTF_KEYUP       = 0x0002
)

// Virtual key codes
const (
	VK_LWIN     = 0x5B
	VK_LCONTROL = 0xA2
	VK_LMENU    = 0xA4  // Left ALT
	VK_TAB      = 0x09
	VK_LEFT     = 0x25
	VK_UP       = 0x26
	VK_RIGHT    = 0x27
	VK_DOWN     = 0x28
	VK_BACK     = 0x08  // BACKSPACE
	VK_RETURN   = 0x0D  // ENTER
)

type POINT struct {
	X int32
	Y int32
}

type MouseInput struct {
	Type uint32
	Mi   MOUSEINPUT
}

type MOUSEINPUT struct {
	Dx          int32
	Dy          int32
	MouseData   uint32
	DwFlags     uint32
	Time        uint32
	DwExtraInfo uintptr
}

var (
	user32                = syscall.NewLazyDLL("user32.dll")
	procSetCursorPos      = user32.NewProc("SetCursorPos")
	procGetCursorPos      = user32.NewProc("GetCursorPos")
	procMouseEvent        = user32.NewProc("mouse_event")
	procKeyboardEvent     = user32.NewProc("keybd_event")
	procVkKeyScanA        = user32.NewProc("VkKeyScanA")
	virtualKeyCodeMapping = make(map[SpecialButtons]uint16)
)

func ClickMouse(in MouseInstruction) error {
	var flag uint32

	switch in {
	case LeftBtn:
		flag = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP
	case RightBtn:
		flag = MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP
	case LeftBtnDown:
		flag = MOUSEEVENTF_LEFTDOWN
	case LeftBtnUp:
		flag = MOUSEEVENTF_LEFTUP
	case RightBtnDown:
		flag = MOUSEEVENTF_RIGHTDOWN
	case RightBtnUp:
		flag = MOUSEEVENTF_RIGHTUP
	}

	_, _, err := procMouseEvent.Call(uintptr(flag), 0, 0, 0, 0)
	if err != syscall.Errno(0) {
		return err
	}
	return nil
}

func MoveMouseRelative(x int, y int) error {
	var pt POINT

	// Get current cursor position
	_, _, err := procGetCursorPos.Call(uintptr(unsafe.Pointer(&pt)))
	if err != syscall.Errno(0) {
		return err
	}

	// Set new cursor position (relative to current)
	ret, _, err := procSetCursorPos.Call(
		uintptr(int32(pt.X) + int32(x)),
		uintptr(int32(pt.Y) + int32(y)),
	)

	if ret == 0 {
		return fmt.Errorf("SetCursorPos failed: %v", err)
	}

	return nil
}

// vkKeyScan converts an ASCII character to a virtual key code
func vkKeyScan(ch byte) uint16 {
	ret, _, _ := procVkKeyScanA.Call(uintptr(ch))
	return uint16(ret)
}

func InitMouse() error {
	virtualKeyCodeMapping[WinBtn] = VK_LWIN
	virtualKeyCodeMapping[CtrlBtn] = VK_LCONTROL
	virtualKeyCodeMapping[AltBtn] = VK_LMENU
	virtualKeyCodeMapping[TabBtn] = VK_TAB
	virtualKeyCodeMapping[ArrLeft] = VK_LEFT
	virtualKeyCodeMapping[ArrUp] = VK_UP
	virtualKeyCodeMapping[ArrRight] = VK_RIGHT
	virtualKeyCodeMapping[ArrDown] = VK_DOWN
	virtualKeyCodeMapping[BackSpace] = VK_BACK
	virtualKeyCodeMapping[Enter] = VK_RETURN
	return nil
}

func TypeText(text string) error {
	for _, char := range text {
		// For non-ASCII characters, this is a simplification and might not work for all characters
		if char > 255 {
			// Skip characters that are outside of ASCII/extended ASCII range
			continue
		}

		// Get virtual key code from character
		vkCode := vkKeyScan(byte(char))

		// Press key
		_, _, err := procKeyboardEvent.Call(
			uintptr(vkCode & 0xFF), // Lower byte contains the virtual key code
			uintptr(0),
			uintptr(0),
			0,
		)
		if err != syscall.Errno(0) {
			return err
		}

		// Release key
		_, _, err = procKeyboardEvent.Call(
			uintptr(vkCode & 0xFF),
			uintptr(0),
			uintptr(KEYEVENTF_KEYUP),
			0,
		)
		if err != syscall.Errno(0) {
			return err
		}
	}

	return nil
}

type TWinBtns map[SpecialButtons]string

var WinBtns map[SpecialButtons]string

func PressSpecial(btn SpecialButtons, statuses ...bool) error {
	// Get the virtual key code for the special button
	vk, ok := virtualKeyCodeMapping[btn]
	if !ok {
		return fmt.Errorf("unknown special button: %d", btn)
	}

	// Default behavior is to press and release
	down := true
	up := true

	// If statuses are provided, they indicate whether to press down and/or release
	if len(statuses) > 0 {
		down = statuses[0]
		if len(statuses) > 1 {
			up = statuses[1]
		}
	}

	// Handle special key flags for certain keys
	flags := uint32(0)
	// Some keys need the extended key flag
	if btn == ArrLeft || btn == ArrUp || btn == ArrRight || btn == ArrDown {
		flags |= KEYEVENTF_EXTENDEDKEY
	}

	// Press the key down if requested
	if down {
		_, _, err := procKeyboardEvent.Call(
			uintptr(vk),
			uintptr(0),
			uintptr(flags),
			0,
		)
		if err != syscall.Errno(0) {
			return err
		}
	}

	// Release the key if requested
	if up {
		_, _, err := procKeyboardEvent.Call(
			uintptr(vk),
			uintptr(0),
			uintptr(flags | KEYEVENTF_KEYUP),
			0,
		)
		if err != syscall.Errno(0) {
			return err
		}
	}

	return nil
}
