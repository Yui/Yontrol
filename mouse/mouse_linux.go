//go:build linux

package mouse

import (
	"fmt"
)

func ClickMouse(in MouseInstruction) error {
	btn := 0

	switch in {
	case LeftBtn:
		btn = 0
	case RightBtn:
		btn = 1
	case LeftBtnDown:
		btn = 40
	case LeftBtnUp:
		btn = 80
	case RightBtnDown:
		btn = 41
	case RightBtnUp:
		btn = 81
	}

	return ClickMouseYDO(btn)
}

func MoveMouseRelative(x int, y int) error {
	return MoveMouseRelativeYDO(x, y)
}

func InitMouse() error {
	LinuxBtns = make(map[SpecialButtons]string)
	// /usr/include/linux/input-event-codes.h
	LinuxBtns[SpecialButtons(0)] = "125" // Win
	LinuxBtns[SpecialButtons(1)] = "29"  // Ctrl
	LinuxBtns[SpecialButtons(2)] = "56"  // Alt
	LinuxBtns[SpecialButtons(3)] = "15"  // Tab
	LinuxBtns[SpecialButtons(4)] = "105" // Left
	LinuxBtns[SpecialButtons(5)] = "103" // Up
	LinuxBtns[SpecialButtons(6)] = "106" // Right
	LinuxBtns[SpecialButtons(7)] = "108" // Down
	LinuxBtns[SpecialButtons(8)] = "14"  // Backspace
	LinuxBtns[SpecialButtons(9)] = "28"  // Enter
	return nil
}

func TypeText(text string) error {
	return TypeTextYDO(text)
}

type TLinuxBtns map[SpecialButtons]string

var LinuxBtns map[SpecialButtons]string

/* true = down, false = up */
func PressSpecial(btn SpecialButtons, statuses ...bool) error {
	btnCode, ok := LinuxBtns[btn]

	if !ok {
		fmt.Println("Undefined key", btnCode)
		return fmt.Errorf("invalid key")
	}

	if len(statuses) > 0 {
		btnStatus := ""
		if statuses[0] {
			btnStatus = "1"
		} else {
			btnStatus = "0"
		}

		// up/down the key
		return TypeSpecialYDO([]string{btnCode + ":" + btnStatus})
	}

	// Press they key (down then up)
	return TypeSpecialYDO([]string{btnCode + ":1", btnCode + ":0"})
}
