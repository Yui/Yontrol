package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"

	"github.com/caddyserver/certmagic" // go get -u github.com/caddyserver/certmagic
	"github.com/gorilla/mux"           // go get github.com/gorilla/mux

	// go get -u github.com/joho/godotenv
	"yaikawa.com/control/mouse"

	// for dns challenge
	"github.com/libdns/njalla" // go get -u github.com/libdns/njalla
)

func main() {
	flag.BoolVar(&ForceHTTP, "forcehttp", false, "Force using HTTP protocol")
	flag.BoolVar(&NoAuth, "noauth", false, "Disable authentication")
	flag.BoolVar(&LiveMousepad, "livemousepad", false, "Use react dev server")
	flag.Parse()

	LoadConfig()

	err := mouse.InitMouse()
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/commands/{name}", handleCommand)
	router.HandleFunc("/commandlist", handleCommandList)
	router.HandleFunc("/ws", handleWS)

	if LiveMousepad {
		react_link, err := url.Parse(Config.General.ReactDevServer)
		if err != nil {
			fmt.Println("Error parsing live mousepad url", err)
			os.Exit(1)
		}

		router.PathPrefix("/").Handler(httputil.NewSingleHostReverseProxy(react_link))
	} else {
		router.PathPrefix("/").Handler(http.FileServer(http.Dir("./mousepad/build/")))
	}

	if !NoAuth {
		router.Use(authMiddleware)
	}

	// TODO: test this (TLS using custom cert files)
	if !ForceHTTP && Config.TLS.CertFile != "" && Config.TLS.CertKey != "" {
		// use TLS via provided certificate

		// TODO: CHECKING EMPTY values by 0 ???? HUH ?????????
		// Allow using custom port here. If not provided --> 443
		if Config.General.Port == 0 {
			Config.General.Port = 443
		}

		fmt.Println("Listening on port " + strconv.FormatUint(uint64(Config.General.Port), 10) + " using HTTPS")
		log.Fatal(http.ListenAndServeTLS(Config.General.Host+":"+strconv.FormatUint(uint64(Config.General.Port), 10), Config.TLS.CertFile, Config.TLS.CertKey, router))
	} else if !ForceHTTP && len(Config.TLS.Domains) > 0 && Config.TLS.Email != "" {
		// Grab certificate via Let's Encrypt
		certmagic.DefaultACME.Email = Config.TLS.Email
		certmagic.DefaultACME.DNS01Solver = &certmagic.DNS01Solver{
			DNSProvider: &njalla.Provider{
				APIToken: Config.TLS.NjallaToken,
			},
		}

		fmt.Println("Listening on port 443 using HTTPS")
		log.Fatal(certmagic.HTTPS(Config.TLS.Domains, router))
	} else {
		// Use http
		if Config.General.Port == 0 {
			Config.General.Port = 8080
		}

		fmt.Println("Listening on port " + strconv.FormatUint(uint64(Config.General.Port), 10) + " using HTTP")
		log.Fatal(http.ListenAndServe(Config.General.Host+":"+strconv.FormatUint(uint64(Config.General.Port), 10), router))
	}
}
