package main

type TConfig struct {
	General TGeneralConfigOptions
	TLS     TTLSConfigOptions
	Command []TCommandConfigOptions
}

type TGeneralConfigOptions struct {
	Host           string
	Port           uint16
	Tokens         []string
	ReactDevServer string
}

type TTLSConfigOptions struct {
	Domains     []string
	Email       string
	NjallaToken string
	CertFile    string
	CertKey     string
}

type TCommandConfigOptions struct {
	Name   string
	Binary string
	Args   []string
}

type TParsedCommand struct {
	Binary string
	Args   []string
}

type TParsedCommands map[string]TParsedCommand

type TWSEvent struct {
	EventType string `json:"eventType"`
}

type TMousepadMoveEvent struct {
	EventType string `json:"eventType"`
	X         int    `json:"x"`
	Y         int    `json:"y"`
}

type TMouseClickEvent struct {
	EventType string `json:"eventType"`
	Btn       string `json:"btn"`
}

type TMouseHoldEvent struct {
	EventType string `json:"eventType"`
	Btn       string `json:"btn"`
	Status    bool   `json:"status"`
}

type TTypeEvent struct {
	EventType string `json:"eventType"`
	Text      string `json:"text"`
}

type TTypeSpecialEvent struct {
	EventType string `json:"eventType"`
	Key       int    `json:"key"`
	Status    bool   `json:"status,omitempty"`
}

var (
	Config         TConfig
	ConfigFile     = "./config.toml"
	ParsedCommands TParsedCommands
	CommandList    []string
	ForceHTTP      bool
	NoAuth         bool
	LiveMousepad   bool
)
