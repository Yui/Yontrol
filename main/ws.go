package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"

	"github.com/gorilla/websocket"
	"yaikawa.com/control/mouse"
)

func checkOrigin(r *http.Request) bool {
	// Allow any origin so this works
	// TODO: Why did this work without it before?
	return true
}

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     checkOrigin,
	}
)

func handleWS(w http.ResponseWriter, r *http.Request) {
	// Upgrade the request to a websocket connection
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		// Handle upgrade error
		fmt.Println("Error upgrading websocket", err)
		return
	}
	defer conn.Close()

	// Handle websocket messages
	for {
		// Read message from connection
		_, message, err := conn.ReadMessage()
		if err != nil {
			// Handle read error
			fmt.Println("Error reading websocket message", err)
			break
		}

		var event TWSEvent
		err = json.Unmarshal(message, &event)
		if err != nil {
			fmt.Println("Error parsing JSON:", err)
			return
		}

		fmt.Println("WS Event", event.EventType, string(message))

		if event.EventType == "mousepad" {
			var data TMousepadMoveEvent
			err = json.Unmarshal(message, &data)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			mouse.MoveMouseRelative(int(data.X), int(data.Y))
		} else if event.EventType == "mouse-click" {
			var data TMouseClickEvent
			err = json.Unmarshal(message, &data)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			if data.Btn == "left" {
				mouse.ClickMouse(mouse.LeftBtn)
			} else {
				mouse.ClickMouse(mouse.RightBtn)
			}

		} else if event.EventType == "mouse-hold" {
			var data TMouseHoldEvent
			err = json.Unmarshal(message, &data)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			if data.Btn == "left" {
				if data.Status {
					mouse.ClickMouse(mouse.LeftBtnDown)
				} else {
					mouse.ClickMouse(mouse.LeftBtnUp)
				}
			} else {
				if data.Status {
					mouse.ClickMouse(mouse.RightBtnDown)
				} else {
					mouse.ClickMouse(mouse.RightBtnUp)
				}
			}
		} else if event.EventType == "keyboard-type" {
			var data TTypeEvent
			err = json.Unmarshal(message, &data)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			mouse.TypeText(data.Text)
		} else if event.EventType == "special-key" {
			var data TTypeSpecialEvent
			err = json.Unmarshal(message, &data)
			if err != nil {
				fmt.Println("Error parsing JSON:", err)
				return
			}

			if data.Key < 0 || data.Key > mouse.MaxSpecialButtons {
				fmt.Println("Invalid key:", data.Key)
				return
			}

			// is value present
			if !reflect.ValueOf(data.Status).IsZero() {
				mouse.PressSpecial(mouse.SpecialButtons(data.Key), data.Status)
			} else {
				mouse.PressSpecial(mouse.SpecialButtons(data.Key))
			}
		} else {
			fmt.Println("Error, invalid event type", event)
		}
	}
}
