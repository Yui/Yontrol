package main

import (
	"errors"
	"net/http"
	"slices"
	"strings"
)

func authenticate(r *http.Request) error {
	authHeader := r.Header.Get("Authorization")

	// Handle bearer tokens
	if strings.HasPrefix(authHeader, "Bearer ") {
		token := strings.TrimSpace(strings.SplitN(authHeader, "Bearer ", 2)[1])
		if err := validateToken(token); err != nil {
			return err
		}
		return nil
	}

	// Handle username/token combination
	username, password, ok := r.BasicAuth()

	if !ok {
		return errors.New("missing basic auth")
	}

	if !usernameValid(username) {
		return errors.New("invalid username")
	}

	if err := validateToken(password); err != nil {
		return err
	}

	return nil
}

func validateToken(token string) error {
	if !slices.Contains(Config.General.Tokens, token) {
		return errors.New("invalid credentials")
	}
	return nil
}

func usernameValid(_ string) bool {
	return true
}

func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := authenticate(r); err != nil {
			w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r) // Call the next handler if authenticated
	})
}
