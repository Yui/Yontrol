package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"strings"

	"github.com/gorilla/mux"
)

func handleCommand(w http.ResponseWriter, r *http.Request) {
	fmt.Println("handle command trigger")

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Only GET requests allowed")
		return
	}

	name := mux.Vars(r)["name"]

	// Get the command
	cmd, ok := ParsedCommands[name]

	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Command not found\n")
		return
	}

	//log.Print("cmd ", cmd)

	// Args
	//log.Print("Args ", r.URL.Query())
	for key, values := range r.URL.Query() {
		//log.Print("arg ", key, " "+values[0])

		tmp := []string{}
		for _, cvalues := range cmd.Args {
			tmp = append(tmp, strings.ReplaceAll(cvalues, "%"+key+"%", values[0]))
		}
		cmd.Args = tmp
	}
	//

	log.Println("Running", cmd.Binary, cmd.Args)
	e := exec.Command(cmd.Binary, cmd.Args...)
	out, err := e.CombinedOutput()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error executing command: %v\n", err)
		log.Println("Out", string(out))
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Command '%s' executed successfully:\n%s\n", name, string(out))
}
