package main

import (
	"log"
	"os"

	"github.com/BurntSushi/toml"
)

func LoadConfig() {
	if _, err := os.Stat(ConfigFile); os.IsNotExist(err) {
		log.Fatal("Config file does not exist. (" + ConfigFile + ")")
	}

	if _, err := toml.DecodeFile(ConfigFile, &Config); err != nil {
		log.Fatal("Invalid config: ", err)
	}

	ParsedCommands = make(TParsedCommands)
	for _, cmd := range Config.Command {
		ParsedCommands[cmd.Name] = TParsedCommand{cmd.Binary, cmd.Args}
	}

	CommandList = make([]string, 0, len(ParsedCommands))
	for key := range ParsedCommands {
		CommandList = append(CommandList, key)
	}

	// Handle defaults
	if Config.General.ReactDevServer == "" {
		Config.General.ReactDevServer = "http://localhost:3000"
	}
}
