package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func handleCommandList(w http.ResponseWriter, r *http.Request) {
	fmt.Println("handle commandList trigger")

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Only GET requests allowed")
		return
	}

	// Marshal the struct to JSON
	jsonData, err := json.Marshal(CommandList)
	if err != nil {
		// Handle error
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set content type header
	w.Header().Set("Content-Type", "application/json")

	// Write JSON data to response
	w.Write(jsonData)

	w.WriteHeader(http.StatusOK)
	fmt.Println("Command list done successfully")
}
