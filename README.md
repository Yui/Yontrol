## Yontrol

- remote command execution and mouse control
- **recommended usage**: (from android phone): use browser page for mouse control, [HTTP shortcuts](https://http-shortcuts.rmy.ch/) to trigger commands
- working directory needs the following: `config.toml`, `mousepad`(`mousepad/build`)

### System requirements

#### Linux

- certmagic(https via njalla) stores certificates at `$HOME/.local/share/certmagic` - run with `XDG_DATA_HOME` set to change it
- needs [ydotool](https://github.com/ReimuNotMoe/ydotool) for mouse control
- don't forget to start the ydotool daemon (ydotoold)

#### Windows

- certmagic(https via njalla) stores certificates at `%USERPROFILE%/.local/share`
- needs powershell for mouse control

### Tricks

### Firewall and systemd (linux)

```bash
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --reload

# Allow binary to use low ports (80,443...)
sudo setcap 'cap_net_bind_service=+ep' /home/$(whoami)/.local/bin/yontrol
```

```bash
cat > ~/.config/systemd/user/yontrol.service <<-EOF
[Unit]
Description="Custom remote control"

[Service]
ExecStart=/home/$(whoami)/.local/bin/yontrol
WorkingDirectory=/home/$(whoami)/.config/yontrol
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=graphical-session.target
EOF

systemctl --user enable --now yontrol
```
