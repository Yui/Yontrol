current_dir=$(pwd)

cd main

rm -rf /tmp/yontrol-building
mkdir -p /tmp/yontrol-building

echo "Building for linux"
go build
mv main /tmp/yontrol-building/yontrol

echo "Building for windows"

GOOS=windows go build
mv main.exe /tmp/yontrol-building/yontrol.exe

echo "Building mousepad"
cd ../mousepad
rm -rf build
make build

echo "Copying mousepad"
mkdir -p /tmp/yontrol-building/mousepad
cp -r ./build /tmp/yontrol-building/mousepad/

cd ..

echo "Copying extra files"
cp config.example.toml /tmp/yontrol-building/

echo "Preparing packages"
cd /tmp/yontrol-building
zip -r yontrol_win.zip mousepad yontrol.exe config.example.toml
zip -r yontrol_linux.zip mousepad yontrol config.example.toml

mv yontrol_*.zip $current_dir
rm -rf /tmp/yontrol-building

echo "Done"